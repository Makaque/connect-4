package connectfour.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created on 13/02/2018.
 */
public class Board {


    protected static class BoardProperties {
        public final int height;
        public final int width;
        public final long[] bottom;
        public final long[] top;
        public final long fullExtraTop;
        public final long fullBottom;
        public final long full;

        public BoardProperties(int width, int height) {
            this.height = height;
            this.width = width;
            this.bottom = initBottom(width, height);
            this.top = initTop(width, height);
            this.fullExtraTop = initFullExtraTop(top);
            this.fullBottom = initFullBottom(bottom);
            this.full = (~0L) >>> (Long.SIZE - (width * (height + 1)));
        }

    }

    private static long[] initBottom(int width, int height) {
        long[] b = new long[width];
        for (int i = 0; i < width; i++) {
            b[i] = (1L << ((height + 1) * i));
        }
        return b;
    }

    // Create an array of 'width' number of bit boards each with a 1 in the bit at the top of
    // the column corresponding to it's board number
    private static long[] initTop(int width, int height) {
        long[] t = new long[width];
        for (int i = 0; i < width; i++) {
            t[i] = 1L << ((height - 1) * (i + 1) + (i * 2));
        }
        return t;
    }

    private static long initFullExtraTop(long[] top) {
        long t = 0;
        for (int i = 0; i < top.length; i++) {
            t |= top[i];
        }
        return t << 1;
    }

    private static long initFullBottom(long[] bottom) {
        long b = 0;
        for (int i = 0; i < bottom.length; i++) {
            b |= bottom[i];
        }
        return b;
    }

    public final int turn;
    private final long[] playerBitboards;
    private final byte currentPlayer;
    private final BoardProperties props;


    private Board(byte player, int turn, long p1Bitboard, long p2Bitboard, BoardProperties props) {
        currentPlayer = player;
        this.turn = turn;
        playerBitboards = new long[2];
        this.playerBitboards[0] = p1Bitboard;
        this.playerBitboards[1] = p2Bitboard;
        this.props = props;
    }

    protected Board(byte player, int turn, long[] playerBitboards, BoardProperties props) {
        this(player, turn, playerBitboards[0], playerBitboards[1], props);
    }

    public Board(int width, int height) {
        this((byte) 0, 0, 0L, 0L, new BoardProperties(width, height));
    }

    public int getHeight(){
        return props.height;
    }

    public int getWidth(){
        return props.width;
    }

    public Boolean canPlay(int col) {
        return ((playerBitboards[0] | playerBitboards[1]) & props.top[col]) == 0;
    }

    public Board play(int col) {
        long allPositions = playerBitboards[0] | playerBitboards[1];
        long newPositions = allPositions + props.bottom[col];
        long newCurrentPlayerPosition = (newPositions | allPositions) ^ allPositions;
        long[] newPlayerPositions = {
                playerBitboards[0], playerBitboards[1]
        };
        newPlayerPositions[currentPlayer] = playerBitboards[currentPlayer] | newCurrentPlayerPosition;
        return new Board(switchPlayer(this.currentPlayer), turn + 1, newPlayerPositions, props);
    }

    public TestBoard testOpponentTurn(int col) {
        long allPositions = playerBitboards[0] | playerBitboards[1];
        long newPositions = allPositions + props.bottom[col];
        long newSingleSquarePosition = (newPositions | allPositions) ^ allPositions;
        long[] newPlayerPositions = {
                playerBitboards[0], playerBitboards[1]
        };
        newPlayerPositions[switchPlayer(currentPlayer)] = playerBitboards[switchPlayer(currentPlayer)] | newSingleSquarePosition;
        return new TestBoard(this.currentPlayer, turn + 1, newPlayerPositions, props);
    }

    public static boolean isWin(Board board) {
        // Since currentPlayer switches immediately following any played move
        // checking for a winning position should be done for the player who
        // just played, or 'the other player.'
        long bitBoard = board.playerBitboards[switchPlayer(board.currentPlayer)];
        return checkBackwardDiagonal(bitBoard, board.getHeight())
                || checkForwardDiagonal(bitBoard, board.getHeight())
                || checkHorizontal(bitBoard, board.getHeight())
                || checkVertical(bitBoard);
    }

    public static boolean isDraw(Board board) {
        boolean fullBoard = (board.playerBitboards[0] | board.playerBitboards[1] | board.props.fullExtraTop) >= board.props.full;
        return fullBoard && !isWin(board);
    }

    public static boolean isEndGame(Board board) {
        return isWin(board) || isDraw(board);
    }


    public static Player winner(Board board) {
        return player(switchPlayer(board.currentPlayer));
    }

    private static Player player(byte p) {
        if (p == 0) {
            return Player.PLAYER1;
        } else {
            return Player.PLAYER2;
        }
    }

    public static Player player(Board board) {
        return player(board.currentPlayer);
    }

    public static boolean isThisPlayerTurn(Board board, Player player) {
        return Board.player(board).equals(player);
    }


    public static EndState endState(Board board, Player player) {
        if (Board.isDraw(board)) {
            return EndState.DRAW;
        }
        if (Board.winner(board).equals(player)) {
            return EndState.WIN;
        }
        return EndState.LOSS;
    }

    private static boolean shiftCheck(long board, int shift) {
        long b = board & (board >> shift);
        return (b & (b >> 2 * shift)) != 0;
    }

    private static boolean checkBackwardDiagonal(long board, int height) {
        return shiftCheck(board, height);
    }

    private static boolean checkForwardDiagonal(long board, int height) {
        return shiftCheck(board, height + 2);
    }

    private static boolean checkHorizontal(long board, int height) {
        return shiftCheck(board, height + 1);
    }

    private static boolean checkVertical(long board) {
        return shiftCheck(board, 1);
    }

    private static byte switchPlayer(byte player) {
        return (byte) (player ^ 1);
    }

    private static List<List<Character>> transpose(List<List<Character>> charBoard) {
        List<List<Character>> transposed = new ArrayList<>();
        for (int i = 0; i < charBoard.get(0).size(); i++) {
            List<Character> col = new ArrayList<>();
            for (List<Character> row : charBoard) {
                col.add(row.get(i));
            }
            transposed.add(col);
        }
        return transposed;
    }

    public static List<List<Character>> beautify(Board board) {
        long p1Board = board.playerBitboards[0];
        long p2Board = board.playerBitboards[1];
        List<List<Character>> charBoard = new ArrayList<>();
        for (int i = 0; i < board.getWidth(); i++) {
            List<Character> row = new ArrayList<>();
            for (int j = 0; j < board.getHeight(); j++) {
                boolean p1Pos = (p1Board & 1L) == 1;
                boolean p2Pos = (p2Board & 1L) == 1;
                if (p1Pos) {
                    row.add(0, 'X');
                } else if (p2Pos) {
                    row.add(0, 'O');
                } else {
                    row.add(0, '-');
                }
                p1Board >>= 1;
                p2Board >>= 1;
            }
            p1Board >>= 1;
            p2Board >>= 1;
            charBoard.add(row);
        }
        return transpose(charBoard);
    }

    public static long key(Board board) {
        return board.playerBitboards[0] + (board.playerBitboards[0] | board.playerBitboards[1]) + board.props.fullBottom;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(key(this));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Board board = (Board) o;

        return Board.key(board) == Board.key(this);
    }
}
