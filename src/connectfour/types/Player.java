package connectfour.types;

/**
 * Created on 15/02/2018.
 */
public enum Player {
    PLAYER1, PLAYER2;

    public static Player otherPlayer(Player player){
        if (player.equals(PLAYER1)){
            return PLAYER2;
        }
        return PLAYER1;
    }
}
