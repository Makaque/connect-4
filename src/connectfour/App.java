package connectfour;

import connectfour.io.IO;
import connectfour.types.*;
import connectfour.utils.IOMessage;

/**
 * Created on 12/02/2018.
 */
public class App {

    private IOMessage<MultiPair<Board, EndState>> endGameCheck(IO io, Player player, MinMaxPlayer opponent, Board board) {
        if (Board.isEndGame(board)) {
            return new IOMessage.Success<>(new MultiPair<>(board, Board.endState(board, player)));
        } else {
            return play(io, player, opponent, board);
        }
    }

    private IOMessage<Integer> canMove(Board board, Integer move){
        if(board.canPlay(move - 1)){
            return new IOMessage.Success<Integer>(move);
        }
        return new IOMessage.Error<>(new RuntimeException("You cannot play that move now."));
    }

    private IOMessage<MultiPair<Board, EndState>> play(IO io, Player player, MinMaxPlayer opponent, Board board) {
        if (Board.player(board).equals(player)) {
            io.reportMove(board);
            return
                    io.promptMove(board.getWidth())
                            .flatMap(gotMove -> canMove(board, gotMove.payload))
                            .map(gotMove ->
                                    board.play(gotMove.payload - 1)
                            )
                            .flatMap(bMsg -> endGameCheck(io, player, opponent, bMsg.payload))
                            .recoverWith((IOMessage.Error<MultiPair<Board, EndState>> e) -> {
                                io.reportError(e);
                                return play(io, player, opponent, board);
                            });
        } else {
            return endGameCheck(io, player, opponent, MinMaxPlayer.play(board, opponent));
        }
    }

    private IOMessage<MultiPair<Board, EndState>> play(IO io, Args args) {
        MinMaxPlayer opponent = new MinMaxPlayer(Player.otherPlayer(args.player), args.width);
        Board board = new Board(args.width, args.height);
        return play(io, args.player, opponent, board);
    }

    private IOMessage<Player> promptPlayer(IO io){
        return io.promptPlayer().recoverWith(e -> {
            io.reportError(e);
            return promptPlayer(io);
        });
    }

    private IOMessage<Args> promptArgs(IO io){
        return io.promptDimensions()
                .flatMap(dim ->
                    promptPlayer(io).map(p -> new Args(dim.payload.left, dim.payload.right, p.payload))
                )
                .recoverWith(e -> {
                    io.reportError(e);
                    return promptArgs(io);
                });
    }

    public void run(IO io) {
        IOMessage<MultiPair<Board, EndState>> gameResult = new IOMessage.Success<>(null);
        while (gameResult.isSuccess()) {
            io.displayDirections();
            IOMessage<Args> playerMsg = promptArgs(io);
            gameResult =
                    playerMsg.flatMap(args ->
                            play(io, args.payload)
                    );

            gameResult.forEach(res ->
                io.reportEndGame(res.payload.left, res.payload.right)
            );
            if (gameResult.isError()) {
                io.reportError((IOMessage.Error<MultiPair<Board, EndState>>) gameResult);
                gameResult = new IOMessage.Success<>(null);
            } else if (gameResult.isQuit()) {
                io.reportQuit();
            }
        }
    }
}
