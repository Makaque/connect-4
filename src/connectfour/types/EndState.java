package connectfour.types;

public enum EndState {
    WIN, LOSS, DRAW
}
