package connectfour.types;

public class Args {
    public final int width;
    public final int height;
    public final Player player;

    public Args(int width, int height, Player player) {
        this.width = width;
        this.height = height;
        this.player = player;
    }
}
