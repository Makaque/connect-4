package connectfour.io;

import connectfour.types.Board;
import connectfour.types.EndState;
import connectfour.types.Pair;
import connectfour.types.Player;
import connectfour.utils.IOMessage;

/**
 * Created on 12/02/2018.
 */
public interface IO {

    public void displayDirections();

    public IOMessage<Pair<Integer>> promptDimensions();

    public IOMessage<Player> promptPlayer();

    public IOMessage<Integer> promptMove(int maxCol);

    public void reportMove(Board board);

    public void reportEndGame(Board board, EndState state);

    public <A> void reportError(IOMessage.Error<A> error);

    public void reportQuit();

}
