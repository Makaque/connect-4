package connectfour.types;

import connectfour.io.TextStreamIO;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 15/02/2018.
 */
public class MinMaxPlayer {
    public final Player player;
    private final int[] traverseOrder;
    private static final int LOSS = -1;
    private static final int WIN = 1;
    private static final int DRAW = 0;

    private int[] initTraverseOrder(int boardWidth) {
        int[] to = new int[boardWidth];
        for (int i = 0; i < boardWidth; i++) {
            to[i] = (boardWidth / 2) + (((i + 1) / 2) * (-1 + 2 * ((i + 1) % 2))); // From the middle outward
        }
        return to;
    }


    public MinMaxPlayer(Player player, int boardWidth) {
        this.player = player;
        traverseOrder = initTraverseOrder(boardWidth);
    }

    private static int evaluate(Board board, MinMaxPlayer minMaxPlayer) {
        if (Board.isDraw(board)) {
            return DRAW;
        }
        if (Board.winner(board).equals(minMaxPlayer.player)) {
            return WIN;
        }
        return LOSS;
    }


    private static List<Board> subMoves(Board board, MinMaxPlayer player) {
        return subMoveCols(board, player).stream().map(board::play).collect(Collectors.toList());
    }

    private static List<Integer> subMoveCols(Board board, MinMaxPlayer player) {
        List<Integer> moves = new ArrayList<>();
        for (int i = 0; i < board.getWidth(); i++) {
            if (board.canPlay(player.traverseOrder[i])) {
                if(isWinningMove(board, player.traverseOrder[i])){
                    moves.clear();
                    moves.add(player.traverseOrder[i]);
                    return moves;
                }
                if (isPreventLossMove(board, player.traverseOrder[i])) {
                    moves.clear();
                    moves.add(player.traverseOrder[i]);
                    return moves;
                }
                moves.add(player.traverseOrder[i]);
            }
        }
        return moves;
    }


    /**
     * Return number of bits in board including extra top row bit.
     *
     * Function is used to shift bits to before beginning of board.
     * @param board
     * @return Number of bits in board including extra top row of bits.
     */
    private static int boardShift(Board board) {
        return (board.getWidth() * (board.getHeight() + 1));
    }

    /**
     * Compare boards using bits for turn number as the highest bits and bits for board key as the low bits.
     * Sorts by turn number with boards having fewer moves coming first.
     */
    private static Function<Integer, Comparator<Board>> treeSorter = (shift) -> (b1, b2) -> {
        long b1TurnComp = (((long) b1.turn) << shift) | Board.key(b1);
        long b2TurnComp = (((long) b2.turn) << shift) | Board.key(b2);
        return Long.compareUnsigned(b1TurnComp, b2TurnComp);
    };

    /**
     * Add board and board value to TreeMap boardVals. When boardVals reaches 15000000 entries remove the last
     * half.
     * @param boardVals TreeMap from board to value
     * @param board
     * @param val
     */
    private static void add(TreeMap<Board, Integer> boardVals, Board board, Integer val) {
        if (boardVals.size() > 15000000) {
            Board halfBoard = (Board) boardVals.navigableKeySet().toArray()[boardVals.size() / 2];
            boardVals.subMap(boardVals.firstKey(), halfBoard).clear();
        }
        if (!boardVals.containsKey(board)) {
            boardVals.put(board, val);
        } else {
            boardVals.replace(board, val);
        }
    }

    private static boolean isWinningMove(Board board, int move){
        return Board.isWin(board.play(move));
    }

    private static boolean isPreventLossMove(Board board, int move) {
        TestBoard testBoard = board.testOpponentTurn(move);
        return Board.isWin(testBoard);
    }


    private static int play(Board board, TreeMap<Board, Integer> boardVals, MinMaxPlayer minMaxPlayer, int min, int max) {
        if (Board.isEndGame(board)) {
            return evaluate(board, minMaxPlayer);
        }
        if (boardVals.containsKey(board)) {
            return boardVals.get(board);
        }
        if (Board.isThisPlayerTurn(board, minMaxPlayer.player)) {
            int v = min;
            for (Integer move : subMoveCols(board, minMaxPlayer)) {
                int subV = play(board.play(move), boardVals, minMaxPlayer, v, max);
                if (subV > v) v = subV;
                if (v > max) return max;
            }
            add(boardVals, board, v);
            return v;
        } else {
            int v = max;
            for (Integer move : subMoveCols(board, minMaxPlayer)) {
                int subV = play(board.play(move), boardVals, minMaxPlayer, min, v);
                if (subV < v) v = subV;
                if (v < min) return min;
            }
            add(boardVals, board, v);
            return v;
        }
    }

    public static Board play(Board board, MinMaxPlayer minMaxPlayer) {
        TreeMap<Board, Integer> boardVals = new TreeMap<>(treeSorter.apply(boardShift(board)));
        Optional<MultiPair<Board, Integer>> nextMove = subMoves(board, minMaxPlayer).stream()
                .map(b -> new MultiPair<>(b, play(b, boardVals, minMaxPlayer, LOSS, WIN)))
                .max((bp1, bp2) -> bp1.right.compareTo(bp2.right));

        if (nextMove.isPresent()) {
            return nextMove.get().left;
        } else {
            throw new RuntimeException("Attempted move on unplayable board");
        }
    }
}
