package connectfour.types;

/**
 * Created on 02/02/2018.
 */
public class Pair<T> {
    public final T left;
    public final T right;

    public Pair(T left, T right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public String toString() {
        return "Pair(" + left + "," + right + ")";
    }
}
