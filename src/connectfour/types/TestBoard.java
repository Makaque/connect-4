package connectfour.types;

/**
 * Created on 23/02/2018.
 */
public class TestBoard extends Board{

    protected TestBoard(byte player, int turn, long[] playerBitboards, BoardProperties props) {
        super(player, turn, playerBitboards, props);
    }
}
