**********************
*** How to Compile ***
**********************

- In a terminal cd into the project root folder connect-4.

- If the directory 'bin' does not exist at this location, create it with the following command:
mkdir bin

- Enter the following command to compile:
javac -d bin -sourcepath src src/Main.java



*******************************
*** Running the Application ***
*******************************

- In a terminal, navigate to the project root folder connect-4.

- Enter the following command to change to the 'bin' subdirectory.
cd bin

- Enter the following command to begin program execution.
java Main

- On screen prompts will give instructions on program input. I've also included a testInputs.txt file
- containing an example set of program inputs.