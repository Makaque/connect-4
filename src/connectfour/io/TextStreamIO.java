package connectfour.io;

import connectfour.types.Board;
import connectfour.types.EndState;
import connectfour.types.Pair;
import connectfour.types.Player;
import connectfour.utils.IOMessage;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

/**
 * Created on 12/02/2018.
 */
public class TextStreamIO implements IO {
    private final InputStream input;
    private final PrintStream output;
    private final Scanner inputScanner;


    public TextStreamIO(InputStream input, PrintStream output) {
        this.input = input;
        this.output = output;
        this.inputScanner = new Scanner(input);
    }

    private void prompt(String out) {
        output.println(out);
    }

    public boolean isQuit() {
        return inputScanner.hasNext("[qQ]");
    }

    @Override
    public void displayDirections() {
        prompt("To begin a game, enter the width and height of the board you wish to play on.");
        prompt("Then enter 1 or 2 to select the first or second player.");
        prompt("Each turn, you'll enter a number between 1 and the total number of columns to play that column (ordered left to right).");
        prompt("Enter Q any time to quit.");
    }

    private IOMessage<Integer> readNextDimension(){
        if(inputScanner.hasNextInt()){
            return new IOMessage.Success<>(inputScanner.nextInt());
        } else if (isQuit()){
            return new IOMessage.Quit<>();
        }
        inputScanner.next();
        return new IOMessage.Error<>(new RuntimeException("Couldn't read board dimensions"));
    }

    @Override
    public IOMessage<Pair<Integer>> promptDimensions() {
        prompt("Width & height:");
        return readNextDimension()
                .flatMap(width -> readNextDimension()
                        .map(height -> new Pair(width.payload, height.payload)));
    }

    @Override
    public IOMessage<Player> promptPlayer() {
        prompt("Player:");
        if (inputScanner.hasNextInt()) {
            Integer player = inputScanner.nextInt();
            if (player == 1) {
                return new IOMessage.Success<>(Player.PLAYER1);
            } else if (player == 2) {
                return new IOMessage.Success<>(Player.PLAYER2);
            }
            return new IOMessage.Error<>(new RuntimeException("Invalid player entered. Must be 1 or 2."));
        } else if (isQuit()) {
            return new IOMessage.Quit<>();
        }
        inputScanner.next();
        return new IOMessage.Error<>(new RuntimeException("Couldn't read player input."));
    }

    @Override
    public IOMessage<Integer> promptMove(int maxCol) {
        prompt("Column:");
        if (inputScanner.hasNextInt()) {
            Integer move = inputScanner.nextInt();
            if (1 <= move && move <= maxCol) {
                return new IOMessage.Success<>(move);
            }
            return new IOMessage.Error<>(new RuntimeException("Invalid column entered. Must be between 1 and " + maxCol + "."));
        } else if (isQuit()) {
            return new IOMessage.Quit<>();
        }
        inputScanner.next();
        return new IOMessage.Error<>(new RuntimeException("Couldn't read move input."));
    }

    @Override
    public void reportMove(Board board) {
        List<List<Character>> prettyBoard = Board.beautify(board);
        prettyBoard.forEach(row -> {
            String rowLine = row.stream().map(c -> c.toString()).reduce((s, s2) -> s + " " + s2).orElse("");
            prompt(rowLine);
        });
    }

    @Override
    public void reportEndGame(Board board, EndState state) {
        switch (state) {
            case WIN:
                prompt("YOU WON!");
                break;
            case LOSS:
                prompt("YOU LOSE");
                break;
            case DRAW:
                prompt("DRAW");
                break;
        }
        reportMove(board);
    }

    @Override
    public <A> void reportError(IOMessage.Error<A> error) {
        prompt("Encountered an error:");
        prompt(error.t.getMessage());
    }

    @Override
    public void reportQuit() {
        prompt("Shutting down");
    }
}
