import connectfour.io.TextStreamIO;
import connectfour.types.Board;
import connectfour.types.TestBoard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created on 20/02/2018.
 */
class BoardTest {
    @Test
    void canPlay() {
        Board board = new Board(7, 6);
        if (!board.canPlay(0)) {
            fail("Can't play column at all");
        }
        board = board.play(0).play(0).play(0).play(0).play(0).play(0);
//        System.out.println(Board.beautify(board));
        if (board.canPlay(0)) {
            fail("Shouldn't be able to play full column");
        }
        if (Board.isEndGame(board)) {
            fail("Should not be end game");
        }
    }

    @Test
    void play() {
    }

    @Test
    void isWinVert() {
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        Board board = new Board(7, 6);
        board = board.play(0);
        board = board.play(1);
        board = board.play(0);
        board = board.play(1);
        board = board.play(0);
        if (Board.isWin(board)) {
            fail("Should not have won yet");
        }
        if (Board.isEndGame(board)) {
            fail("Should not be end game");
        }
        board = board.play(1);
        if (Board.isWin(board)) {
            fail("Player 2 should not win");
        }
        board = board.play(0);
//        System.out.println(Board.beautify(board));
//        io.reportMove(board);
        if (!Board.isWin(board)) {
            fail("Should be a win");
        }
        if (!Board.isEndGame(board)) {
            fail("Should be end game");
        }

    }

    @Test
    void isWinHoriz() {
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        Board board = new Board(7, 6);
        board = board.play(0);
        board = board.play(0);
        board = board.play(1);
        board = board.play(1);
        board = board.play(2);
        board = board.play(2);
        if (Board.isWin(board)) {
            fail("Should not have won yet");
        }
        if (Board.isEndGame(board)) {
            fail("Should not be end game");
        }
        board = board.play(3);
//        System.out.println(Board.beautify(board));
//        io.reportMove(board);
        if (!Board.isWin(board)) {
            fail("Should be a win");
        }
        if (!Board.isEndGame(board)) {
            fail("Should be end game");
        }

    }

    Board showBoard(Board board) {
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        io.reportMove(board);
        System.out.println();
        return board;
    }

    @Test
    void isWinDiagForward() {
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        Board board = new Board(7, 6);
        board = showBoard(board.play(0));
        board = showBoard(board.play(1));
        board = showBoard(board.play(1));
        board = showBoard(board.play(2));
        board = showBoard(board.play(2));
        board = showBoard(board.play(3));
        board = showBoard(board.play(2));
        if (Board.isWin(board)) {
            fail("Should not have won yet");
        }
        if (Board.isEndGame(board)) {
            fail("Should not be end game");
        }
        board = showBoard(board.play(3));
        board = showBoard(board.play(3));
        board = showBoard(board.play(5));
        if (Board.isWin(board)) {
            fail("Should still not have won yet");
        }
        board = showBoard(board.play(3));
//        System.out.println(Board.beautify(board));
//        io.reportMove(board);
        if (!Board.isWin(board)) {
            fail("Should be a win");
        }
        if (!Board.isEndGame(board)) {
            fail("Should be end game");
        }

    }

    @Test
    void isWinDiagBackward() {
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        Board board = new Board(7, 6);
        board = showBoard(board.play(6 - 0));
        board = showBoard(board.play(6 - 1));
        board = showBoard(board.play(6 - 1));
        board = showBoard(board.play(6 - 2));
        board = showBoard(board.play(6 - 2));
        board = showBoard(board.play(6 - 3));
        board = showBoard(board.play(6 - 2));
        if (Board.isWin(board)) {
            fail("Should not have won yet");
        }
        if (Board.isEndGame(board)) {
            fail("Should not be end game");
        }
        board = showBoard(board.play(6 - 3));
        board = showBoard(board.play(6 - 3));
        board = showBoard(board.play(6 - 5));
        if (Board.isWin(board)) {
            fail("Should still not have won yet");
        }
        board = showBoard(board.play(6 - 3));
//        System.out.println(Board.beautify(board));
//        io.reportMove(board);
        if (!Board.isWin(board)) {
            fail("Should be a win");
        }
        if (!Board.isEndGame(board)) {
            fail("Should be end game");
        }

    }

    @Test
    void isDraw() {
        int[] move = {
                0, 1,
                0, 1,
                1, 0,
                1, 0,
                0, 1,
                0, 1,
        };
        Board board = new Board(7, 6);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < move.length; j++) {
                board = showBoard(board.play(move[j] + 2 * i));
            }
        }
        if (Board.isDraw(board)) {
            fail("Should not be draw yet");
        }
        if (Board.isEndGame(board)) {
            fail("Should not be end game");
        }
        board = showBoard(board.play(6));
        board = showBoard(board.play(6));
        board = showBoard(board.play(6));
        board = showBoard(board.play(6));
        board = showBoard(board.play(6));
        board = showBoard(board.play(6));
        if (!Board.isDraw(board)) {
            fail("Should be a draw");
        }
        if (!Board.isEndGame(board)) {
            fail("Should be end game");
        }
    }

    @Test
    void isDraw2() {
        int[] move = {
                1, 2,
                2, 2,
                4, 2,
                4, 1,
                0, 1,
                4, 4,
                3, 1,
                0, 3,
                0, 0,
                3, 3,
        };
        Board board = new Board(5, 4);
        for (int i = 0; i < move.length; i++) {
            board = board.play(move[i]);
        }
        if (Board.isDraw(board)) {
            showBoard(board);
            fail("Should not be draw");
        }
        if (!Board.isEndGame(board)) {
            showBoard(board);
            fail("Should be end game");
        }
        if(!Board.isWin(board)){
            showBoard(board);
            fail("Should be win");
        }
    }


    @Test
    void testOpponentTurn() {
        Board board = new Board(7, 6);

        board = board.play(0);
        board = board.play(1);

        board = board.play(0);
        board = board.play(1);

        board = board.play(0);

        TestBoard testBoard = board.testOpponentTurn(0);
        showBoard(board);
        showBoard(testBoard);

        if (!Board.isWin(testBoard)) {
            fail("Should be win for opponent");
        }
    }

    @Test
    void winner() {
    }

    @Test
    void player() {
    }

    @Test
    void isThisPlayerTurn() {
    }

    @Test
    void endState() {
    }

    @Test
    void beautify() {
    }

}