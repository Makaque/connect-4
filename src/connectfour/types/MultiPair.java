package connectfour.types;

public class MultiPair<T,U> {
    public final T left;
    public final U right;

    public MultiPair(T left, U right) {
        this.left = left;
        this.right = right;
    }
}
